// Include gulp
var gulp = require("gulp");

// Include Plugins
var rename = require("gulp-rename");
var sass = require("gulp-sass");
var cssnano = require("gulp-cssnano");
var concatcss = require("gulp-concat-css");
var concatjs = require("gulp-concat");
var uglifyjs = require("gulp-uglify");

// Compile Sass
gulp.task("sass", function() {
    return gulp.src("public/css/src/*.scss")
        .pipe(sass.sync())
	.pipe(concatcss("main.css"))
	.pipe(cssnano())
        .pipe(gulp.dest("public/css/dist"));
});

// Concatenate & Minify JS
gulp.task("js", function() {
    return gulp.src("public/js/src/*.js")
        .pipe(concatjs("main.js"))
        .pipe(uglifyjs())
        .pipe(gulp.dest("public/js/dist"));
});

// Watch Files For Changes
gulp.task("watch", function() {
    gulp.watch(["public/css/src/*.scss"], ["sass"]);
    gulp.watch(["public/js/src/*.js"], ["js"]);
});

// Default Task
gulp.task("default", gulp.parallel(
    "sass",
    "js"
));
