<?php

$root = filter_input(INPUT_SERVER, "DOCUMENT_ROOT");

require $root."/../vendor/autoload.php";

$frontController = new \Skeleton\Misc\Bootstrap();

$frontController->init();
