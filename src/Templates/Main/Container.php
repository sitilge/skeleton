<!DOCTYPE html>
<html prefix="og: http://ogp.me/ns#" lang="en">
<head>
	<title></title>
	<meta charset="utf-8">
	<meta content="IE=edge" http-equiv="X-UA-Compatible">
	<meta content="width=device-width, initial-scale=1" name="viewport">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<meta content="" property="og:title">
	<meta content="" property="og:image">
	<meta content="" property="og:description">
	<link rel="stylesheet" href="<?php echo $url->makeUrl(); ?>css/dist/main.css">
</head>
<body>
	<h1>Hello, world!</h1>
	<script src="<?php echo $url->makeUrl(); ?>js/dist/main.js"></script>
</body>
</html>
