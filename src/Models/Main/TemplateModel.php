<?php

namespace Skeleton\Models\Main;

class TemplateModel
{
	public $data = [];

	public $file = '';

	/**
	 * Set a file.
	 */
	public function file($file)
	{
		$this->file = $file;

		if (empty(pathinfo($file, PATHINFO_EXTENSION))) {
			$this->file .= ".php";
		}

		return $this;
	}

	/**
	 * Set a parameter.
	 */
	public function set($key, $value = null)
	{
		if (is_array($key)) {
			foreach ($key as $name => $value) {
				$this->data[$name] = $value;
			}

			return $this;
		}

		$this->data[$key] = $value;

		return $this;
	}

	/**
	 * Get a parameter.
	 */
	public function get($key)
	{
		return isset($this->data[$key]) ? $this->data[$key] : null;
	}

	/**
	 * Render a template.
	 */
	public function render($input = null)
	{
		ob_start();

		extract($this->data, EXTR_SKIP);

		if (null !== $input) {
			echo $input;

			return ob_get_clean();
		}

		require $this->file;

		return ob_get_clean();
	}
}
