<?php

namespace Skeleton\Models\Main;

class UrlModel
{
	/**
	 * Make a route.
	 */
	public function makeUrl($segments = [])
	{
		$root = filter_input(INPUT_SERVER, "DOCUMENT_ROOT");

		$config = require $root."/../src/Config/Main.php";

		$pattern = [];

		foreach ($segments as $index => $segment) {
			if ('' !== $segment) {
				$pattern[] = "%s";
			} else {
				unset($segments[$index]);
			}
		}

		return rtrim($config["fqdn"], '/').'/'.$this->prepare(implode('/', $pattern), $segments);
	}

	/**
	 * Prepare a pattern.
	 */
	private function prepare($pattern, $segments)
	{
		foreach ($segments as &$argument) {
			$argument = preg_replace("#[^a-zA-Z0-9_\-]#", '', str_replace(' ', '_', $argument));
		}

		return vsprintf($pattern, $segments);
	}

	/**
	 * Get a segment.
	 */
	public function getSegment($index = 0)
	{
		$url = trim($this->getUrl(), '/');

		$segments = explode('/', $url);
		$count = count($segments);

		if ($count <= $index || $index < 0) {
			return $segments[$count - 1];
		}

		return $segments[$index];
	}

	/**
	 * Get the current url.
	 */
	public function getUrl()
	{
		$server = filter_input_array(INPUT_SERVER);

		if (!empty($server["PATH_INFO"])) {
			return rawurldecode(parse_url($server["PATH_INFO"], PHP_URL_PATH));
		} elseif (!empty($server["REQUEST_URI"])) {
			return rawurldecode(parse_url($server["REQUEST_URI"], PHP_URL_PATH));
		}

		return false;
	}
}
