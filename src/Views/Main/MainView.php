<?php

namespace Skeleton\Views\Main;

use Skeleton\Models\Main\TemplateModel;
use Skeleton\Models\Main\UrlModel;

class MainView
{
	public $segments;

	/**
	 * MainView constructor.
	 */
	public function __construct(
		$segments = [],
		TemplateModel $templateModel = null,
		UrlModel $urlModel = null
	) {
		$this->segments = $segments;
		$this->templateModel = $templateModel;
		$this->urlModel = $urlModel;
	}

	/**
	 * Manage the output.
	 */
	public function manageOutput()
	{
		$root = filter_input(INPUT_SERVER, "DOCUMENT_ROOT");

		echo $this->templateModel
			->set("url", $this->urlModel)
			->file($root."/../src/Templates/Main/Container.php")
			->render();
	}
}
