<?php

namespace Skeleton\Misc;

use FastRoute\RouteCollector;
use Whoops\Handler\Handler;
use Whoops\Handler\PrettyPageHandler;
use Whoops\Handler\JsonResponseHandler;
use Whoops\Run;

class Bootstrap
{
	/**
	 * The initial method called.
	 */
	public function init()
	{
		$this->initThrowable();

		$this->initSession();

		$this->initRoute();
	}

	/**
	 * Initialize the throwable.
	 */
	private function initThrowable()
	{
		$root = filter_input(INPUT_SERVER, "DOCUMENT_ROOT");

		$config = require $root."/../src/Config/Main.php";

		if (empty($config["development"])) {
			$this->initHandler(function($exception, $inspector, $run) use ($config) {
				call_user_func_array($config["callable"], [
					$exception,
					$inspector,
					$run
				]);

				return Handler::DONE;
			});

			return;
		}

		$server = filter_input_array(INPUT_SERVER);

		if (!empty($server["HTTP_X_REQUESTED_WITH"])
			&& strtolower($server["HTTP_X_REQUESTED_WITH"]) == "xmlhttprequest") {
			$this->initHandler(new JsonResponseHandler());

			return;
		}

		$this->initHandler(new PrettyPageHandler());
	}

	/**
	 * Initialize the session.
	 */
	private function initSession()
	{
		session_start();
	}

	/**
	 * Initialize the throwable handler.
	 */
	private function initHandler($handler)
	{
		$run = new Run();

		$run->pushHandler($handler);

		$run->register();
	}

	/**
	 * Initialize the route handler.
	 */
	private function initRoute()
	{
		$root = filter_input(INPUT_SERVER, "DOCUMENT_ROOT");

		$routes = require $root."/../src/Misc/Routes.php";

		$dispatcher = \FastRoute\simpleDispatcher(function (RouteCollector $collector) use ($routes) {
			foreach ($routes as $route) {
				$collector->addRoute($route[0], $route[1], $route[2]);
			}
		});

		$method = $_SERVER["REQUEST_METHOD"];
		$uri = $_SERVER["REQUEST_URI"];

		if (false !== $position = strpos($uri, '?')) {
			$uri = substr($uri, 0, $position);
		}

		$uri = rawurldecode($uri);

		$route = $dispatcher->dispatch($method, $uri);

		switch ($route[0]) {
		case \FastRoute\Dispatcher::NOT_FOUND:
			throw new \ErrorException("Route $method $uri not found.", 404);
			break;
		case \FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
			throw new \ErrorException("Method $method not allowed.", 405);
			break;
		case \FastRoute\Dispatcher::FOUND:
			$handler = $route[1];
			$arguments = $route[2];

			call_user_func_array($handler, $arguments);
		}
	}
}
