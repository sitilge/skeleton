<?php

use Skeleton\Models\Main\TemplateModel;
use Skeleton\Models\Main\UrlModel;
use Skeleton\Views\Main\MainView;

return [
	[
		["GET"],
		'/',
		function(...$segments) {
			$templateModel = new TemplateModel();
			$urlModel = new UrlModel();

			$mainView = new MainView();

			$mainView->segments = $segments;

			$mainView->templateModel = $templateModel;
			$mainView->urlModel = $urlModel;

			$mainView->manageOutput();
		}
	]
];
