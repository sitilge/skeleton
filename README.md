# skeleton

A simple MVC skeleton for php apps.

## Requirements

- `php` >= 5.6
- `composer`
- `npm`, optionally
- `gulp`, optionally

## Installation

When inside the project directory

- Run `composer install`
- Create a config file named `Main.php` under `src/Config/` which contains lines like (set `development` to `false` to disable development mode)

````
<?php

return [
	"fqdn" => "/",
	"development" => true,
	"callable" => function($exception, $inspector, $run) {
		http_response_code($exception->getCode());
		echo "Some error.";

		exit;
	}
];
````

- Run `npm install`, optionally
- Run `gulp`, optionally
